#include "formats.h"
#include "image.h"
#include "status_codes.h"

#include <malloc.h>
#include <stdio.h>

#define BF_TYPE 19778
#define BI_PLANES 1
#define BI_BIT_COUNT 24

#define PADDING_FORMULA(width) ((4 - ((width) * SIZE_OF_ONE_PIXEL % 4)) % 4)

//reads BMP from file and provides an image object
enum read_image_status read_bmp(FILE *file, image *img) {
    struct bmp_header headers;
    if(!fread(&headers, sizeof(headers), 1, file)) {
        return READ_IMAGE_ERROR;
    }

    if (headers.bfType != BF_TYPE || headers.biPlanes != BI_PLANES || headers.biBitCount != BI_BIT_COUNT) {
        return READ_IMAGE_ERROR;
    }

    free(img->data);
    struct pixel *image_data = (struct pixel *) malloc(headers.biWidth * headers.biHeight * SIZE_OF_ONE_PIXEL);
    if (image_data == NULL) {return READ_IMAGE_ERROR;}
    uint32_t padding_bytes = PADDING_FORMULA(headers.biWidth);

    if (fseek(file, headers.bOffBits - (long) sizeof(headers), SEEK_CUR)) { //skip offset between headers and data
        free(image_data);
        return READ_IMAGE_ERROR;
    }

    size_t current_pos = 0;
    for (uint32_t line_number = 0; line_number < headers.biHeight; line_number++) {
        if (fread(image_data + current_pos, headers.biWidth * SIZE_OF_ONE_PIXEL, 1, file) != 1 
            || fseek(file, padding_bytes, SEEK_CUR)) {
            free(image_data);
            return READ_IMAGE_ERROR;
        }        
        current_pos += headers.biWidth;
    }

    img->height = headers.biHeight;
    img->width = headers.biWidth;
    img->data = image_data;

    return READ_IMAGE_OK;
}

//writes given image to specified file as BMP
enum write_image_status write_bmp(FILE *file, image *img) {
    uint32_t padding_bytes = PADDING_FORMULA(img->width);

    struct bmp_header headers = {
            .biWidth = img->width,
            .biHeight = img->height,
            .bOffBits = sizeof(struct bmp_header),
            .bfType =  BF_TYPE,
            .bfileSize = sizeof(struct bmp_header) + (img->width * SIZE_OF_ONE_PIXEL + padding_bytes) * img->height,
            .biPlanes = BI_PLANES,
            .biBitCount = BI_BIT_COUNT
    };

    if (!fwrite(&headers, sizeof(headers), 1, file)) {
        return WRITE_IMAGE_ERROR;
    }
    size_t current_pos = 0;
    for (uint32_t line_number = 0; line_number < img->height; line_number++) {
        if (fwrite(img->data + current_pos, img->width * SIZE_OF_ONE_PIXEL, 1, file) != 1 
            || fseek(file, padding_bytes, SEEK_CUR)) {
            return WRITE_IMAGE_ERROR;
        }    
        current_pos += img->width;
    }
    return WRITE_IMAGE_OK;
}

