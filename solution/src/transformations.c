#include "image.h"

#include <malloc.h>
#include <string.h>

#define APPLY_TRANSFORMATION(transformation)                                        \
    struct pixel* new_data = (struct pixel*) malloc(old->width * old->height * 3);  \
    if (new_data == NULL) {return NULL;}                                            \
    for (size_t i = 0; i < old->height; i++) {                                      \
        for (size_t j = 0; j < old->width; j++) {                                   \
            transformation                                                          \
        }                                                                           \
    }                                                                               \
    free(old->data);                                                                \
    old->data = new_data;

// returns the same image but rotated on specified angle
image* rotate_multiple_of_90(image* old, int angle) {
    if (angle < 0) {angle += 360 * (-angle / 360 + 1);}
    angle %= 360;
    switch (angle) {
        case 0: {
            return old;
        }
        case 270: {
            uint64_t new_width = old->height;
            uint64_t new_height = old->width;
            // [i, j] = width * i + j
            // [i,j] --> [j, new_width - 1 - i]
            APPLY_TRANSFORMATION(new_data[new_width * j + new_width - 1 - i] = old->data[old->width * i + j];)
            old->width = new_width;
            old->height = new_height;
            return old;
        }
        case 180: {
            // [i,j] --> [new_height - 1 - i, new_width - 1 - j]
            APPLY_TRANSFORMATION(new_data[old->width * (old->height - 1 - i) + old->width - 1 - j] = old->data[old->width * i + j];)
            return old;
        }
        case 90: {
            uint64_t new_width = old->height;
            uint64_t new_height = old->width;
            // [i,j] --> [new_height - 1 - j, i]
            APPLY_TRANSFORMATION(new_data[new_width * (new_height - 1 - j) + i] = old->data[old->width * i + j];)
            old->width = new_width;
            old->height = new_height;
            return old;
        }
        default: {
            return NULL;
        }
    }
}
