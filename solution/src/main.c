#include "formats.h"
#include "image.h"
#include "status_codes.h"
#include "transformations.h"

#include <stdio.h>
#include <stdlib.h>

enum process_status process_rotation(char* file_in_name, char* file_out_name, char* ang) {
    //0, 90, -90, 180, -180, 270, -270
    char *end;
    int angle = (int) strtol(ang, &end, 10);

    FILE* file_in = fopen(file_in_name, "rb");

    if (!file_in) {
        return PROCESS_READ_FILE_ERROR;
    }

    FILE* file_out = fopen(file_out_name, "w");

    if (!file_out) {
        fclose(file_in);
        return PROCESS_WRITE_FILE_ERROR;
    }

    image img = {0};
    if (read_bmp(file_in, &img) != READ_IMAGE_OK) {
        fclose(file_in);
        fclose(file_out);
        return PROCESS_PARSE_IMAGE_ERROR;
    }

    image* new = rotate_multiple_of_90(&img, angle);

    if (write_bmp(file_out, new) != WRITE_IMAGE_OK) {
        free(new->data);
        fclose(file_in);
        fclose(file_out);
        return PROCESS_WRITE_FILE_ERROR;
    }

    free(new->data);

    fclose(file_in);
    fclose(file_out);

    return PROCESS_OK;
}

//Input format: ./image-transformer <source-image> <transformed-image> <angle>"
int main( int argc, char** argv ) {
    if (argc < 4) {
        fprintf(stderr, "argument amount error");
        return -1;
    }

    enum process_status status = process_rotation(argv[1], argv[2], argv[3]);

    switch (status) {
        case PROCESS_READ_FILE_ERROR: {
            fprintf(stderr, "error in reading file");
            break;
        }
        case PROCESS_WRITE_FILE_ERROR: {
            fprintf(stderr, "error in writing to file");
            break;
        }
        case PROCESS_PARSE_IMAGE_ERROR: {
            fprintf(stderr, "error in parsing image");
            break;
        }
        default: {}
    }

    return 0;
}


