#ifndef IMAGE_TRANSFORMER_STATUS_CODES_H
#define IMAGE_TRANSFORMER_STATUS_CODES_H

enum process_status {
    PROCESS_OK = 0,
    PROCESS_READ_FILE_ERROR,
    PROCESS_WRITE_FILE_ERROR,
    PROCESS_PARSE_IMAGE_ERROR
};

enum read_image_status {
    READ_IMAGE_OK = 0,
    READ_IMAGE_ERROR
};

enum write_image_status {
    WRITE_IMAGE_OK = 0,
    WRITE_IMAGE_ERROR
};

#endif //IMAGE_TRANSFORMER_STATUS_CODES_H
