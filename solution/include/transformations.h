#ifndef IMAGE_TRANSFORMER_TRANSFORMATIONS_H
#define IMAGE_TRANSFORMER_TRANSFORMATIONS_H

#include "image.h"

image* rotate_multiple_of_90(image* old, int angle);

#endif //IMAGE_TRANSFORMER_TRANSFORMATIONS_H
