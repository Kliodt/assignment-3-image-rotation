#ifndef IMAGE_TRANSFORMER_IMAGE_H
#define IMAGE_TRANSFORMER_IMAGE_H

#include <stdint.h>

#define SIZE_OF_ONE_PIXEL sizeof(struct pixel)

typedef struct image {
    uint64_t width, height;
    struct pixel* data;
} image;

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

#endif //IMAGE_TRANSFORMER_IMAGE_H
